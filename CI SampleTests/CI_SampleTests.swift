//
//  CI_SampleTests.swift
//  CI SampleTests
//
//  Created by Roland Heusser on 11/3/18.
//  Copyright © 2018 Sinklair Studios. All rights reserved.
//

import XCTest
@testable import CI_Sample

class CI_SampleTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class. 
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
